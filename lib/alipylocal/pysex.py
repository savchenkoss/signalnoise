from math import hypot
from pathlib import Path
import subprocess
from .. import asciidatalocal as asciidata
from numpy import genfromtxt, argsort

path_to_sex_config = Path(__file__).parent.parent / "alipysex.sex"
path_to_sex_param = Path(__file__).parent.parent / "alipysex.param"
path_to_sex_conv = Path(__file__).parent.parent / "alipysex.conv"
path_to_sex_nnw = Path(__file__).parent.parent / "alipysex.nnw"
workdir = Path(__file__).parent.parent.parent / "workDir"


def _get_cmd(img, conf_args):
    cmd = f"sex {img} -c {path_to_sex_config} "
    cmd += f"-VERBOSE_TYPE QUIET -PARAMETERS_NAME {path_to_sex_param} "
    cmd += f"-FILTER_NAME {path_to_sex_conv} -STARNNW_NAME {path_to_sex_nnw} "
    args = [''.join(['-', key, ' ', str(conf_args[key])]) for key in conf_args]
    cmd += ' '.join(args)
    return cmd


def filterPolCat(catInName, catOutName, polarFilterName, camera):
    matchDist = 2.0
    polarFilterName = polarFilterName.lower()
    polarFilterIdx = camera.polar_filters.index(polarFilterName)
    shift_dx = camera.polar_pair_shift[polarFilterIdx][0]
    shift_dy = camera.polar_pair_shift[polarFilterIdx][1]

    cat = genfromtxt(catInName)
    # Sort catalogue by x-coordinate
    try:
        cat = cat[argsort(cat[:, 1])]
    except IndexError:
        # empty cat
        return None
    pairs = []

    for i, obj in enumerate(cat):
        xObj = obj[1]
        yObj = obj[2]
        xPair = xObj + shift_dx
        yPair = yObj + shift_dy
        minDist = 1e5

        for obj2 in cat[i:]:
            x2 = obj2[1]
            y2 = obj2[2]
            if x2 > xPair + 5:
                # Pair object can not be so far away from the original one
                break
            dist = hypot(x2-xPair, y2-yPair)
            if dist < minDist:
                minDist = dist
                pairObj = obj2
        if minDist < matchDist:
            # possible pair object found
            pairs.append((obj, pairObj))

    fout = open(catOutName, "w")
    fout.truncate(0)
    for line in open(catInName):
        if line.startswith("#"):
            fout.write(line)
    for objNum, pair in enumerate(pairs):
        outStr = " ".join([str(v) for v in pair[0][1:]])
        fout.write("%i %s\n" % (objNum+1, outStr))
    fout.close()


def run(image, params=[], conf_file=None, conf_args={}, polarMode=None, camera=None):
    """
    Run sextractor on the given image with the given parameters.

    image: filename or numpy array
    imageref: optional, filename or numpy array of the the reference image
    params: list of catalog's parameters to be returned
    conf_file: optional, filename of the sextractor catalog to be used
    conf_args: optional, list of arguments to be passed to sextractor (overrides the parameters in the conf file)
    catdir : where to put the cats (default : next to the images)

    Returns an asciidata catalog containing the sextractor output

    Usage exemple:
        import pysex
        cat = pysex.run(myimage, params=['X_IMAGE', 'Y_IMAGE', 'FLUX_APER'], conf_args={'PHOT_APERTURES':5})
        print cat['FLUX_APER']
    """
    if image.suffix == ".cat":
        return asciidata.open(image)

    # Run sex :
    if polarMode is None:
        conf_args['CATALOG_NAME'] = workdir/'alipysex.cat'
    else:
        conf_args['CATALOG_NAME'] = workdir/'alipysex_polar.cat'
    conf_args['PARAMETERS_NAME'] = path_to_sex_param
    cmd = _get_cmd(image, conf_args)
    res = subprocess.call(cmd, shell=True)
    if res:
        print("Error during sextractor execution!")
        return

    # Clean polar data if necessary
    if polarMode is not None:
        filterPolCat(workdir/"alipysex_polar.cat",
                     workdir/"alipysex.cat", polarMode, camera)
    # Returning the cat:
    return asciidata.open(workdir/"alipysex.cat")
