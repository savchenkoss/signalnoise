#! /usr/bin/env python

from pathlib import Path
import re
import fnmatch
from .camera import Camera

camera = Camera("st7_lx200")
path_to_list_of_objects = (Path(__file__) / ".." / ".." / ".." / "references" / "list_of_objects.dat").resolve()
list_of_objects = []
with open(path_to_list_of_objects) as fin:
    for line in fin:
        list_of_objects.append(line.strip())


def parse_object_file_name(fName):
    """ Function gets object name, add string (if exists) and
    filter name out of object file name"""
    # File name is like objname+addstr+filter+number.FIT,
    # for example for wcom it may be 'wcom2b001.FIT'
    fNameNoExt = fName.stem
    fNameNoNumbers = fNameNoExt[:-3]
    filterCombination = fNameNoNumbers[-1]

    fNameNoFilt = fNameNoNumbers[:-1]
    # Let's find what object is it
    for objName in list_of_objects:
        if fNameNoFilt.startswith(objName):
            addString = fNameNoFilt[len(objName):]
            return objName, filterCombination, addString
    # No sutable object found
    return None, None, None


def get_mode(filterCombination):
    photFiltName, polarFiltName = None, None
    if filterCombination.lower() in camera.phot_filters:
        photFiltName = filterCombination
    elif filterCombination.lower() in camera.polar_filters:
        polarFiltName = filterCombination
    return photFiltName, polarFiltName


def match_files(pathToData, objName, addString, filterCombination):
    """
    Function returns list of files from the directory that look like they were
    captured with this camera
    """
    list_of_all_files = pathToData.glob(f"{objName}*")
    matched_files = []

    fileNamePattern1 = objName + addString + filterCombination + "*.fit"

    template1 = re.compile(fnmatch.translate(fileNamePattern1), re.IGNORECASE)

    for fName in list_of_all_files:
        fNameWithoutPath = fName.name
        if template1.match(fNameWithoutPath) is not None:
            matched_files.append(fName)
    return matched_files
