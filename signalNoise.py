#! /usr/bin/env python

import os
from pathlib import Path
import numpy as np
from collections import OrderedDict
import tkinter as Tk

from astropy.io import fits as fits

from lib import Menu  # noqa
from lib import ImagPanel  # noqa
from lib import RightPanel  # noqa
from lib import alignment  # noqa
from lib import reduction # noqa
from lib import se  # noqa
from lib.cameras import camera  # noqa
from lib import Configuration  # noqa
from lib.filterPolCat import filterPolCat  # noqa


class MainApplication(Tk.Frame):
    def __init__(self, *args, **kwargs):
        self.objName = None
        self.currentObject = None
        self.currentFilterCombination = None
        self.ref = None
        self.rawImages = []
        self.magData = []
        self.filterChecked = False
        self.photFiltName = None
        self.polarFiltName = None
        self.desiredExposures = 0
        self.object_selected_manually = False
        self.photoLog = OrderedDict()
        # Load configuration parameters
        self.params = Configuration.Parameters()
        # Set up camera
        self.cameraName = self.params["camera_name"]
        self.camera = camera.Camera(self.cameraName)
        # Load appropriate module with a file name parser
        import importlib
        self.camera_parser = importlib.import_module(f"lib.cameras.{self.cameraName}")
        # check if there is the working directory and create if nessesary
        self.workdir = Path(__file__).parent / "workDir"
        if not self.workdir:
            os.mkdir(self.workdir)
        else:
            # cleanup working directory
            for f in self.workdir.glob("*.*"):
                try:
                    # File can be protected from removing (for example if it is
                    # opened by another application)
                    os.remove(f)
                except OSError:
                    pass
        # cache flats
        self.cache_flats()

        # GUI stuff:
        self.root = Tk.Tk()
        self.root.title("SignalNoise")
        icon_path = str(Path(__file__).parent / "lib/icon.png")
        photo = Tk.PhotoImage(file=icon_path)
        self.root.wm_iconphoto(False, photo)
        self.root.protocol('WM_DELETE_WINDOW', self.on_closing)
        self.menubar = Menu.MenuBar(self)
        self.imagPanel = ImagPanel.ImagPanel(self)
        self.rightPanel = RightPanel.RightPanel(self)
        self.imagPanel.show_message("Welcome to SignalNoise!\nPlease choose a data directory.")
        self.root.mainloop()

    def on_closing(self):
        self.root.destroy()

    def setup(self, dirName):
        self.dirName = Path(dirName)
        self.cycle()

    def cache_flats(self):
        self.flats = {}
        path_to_flats = Path(self.params["path_to_flats"])
        # Load all flat files from the directory
        for flat_name in path_to_flats.glob("ff*.fts"):
            filt = flat_name.stem[2:].lower()
            hdu = fits.open(flat_name)
            fData = hdu[0].data.astype(float)
            # Flip flat field if needed
            if self.camera.flip_flat_ud is True:
                fData = np.flipud(fData)
            if self.camera.flip_flat_lr is True:
                fData = np.fliplr(fData)
            fData /= np.mean(fData)
            self.flats[filt.lower()] = fData
            if len(filt) == 2:
                # In case user have switched the order of filters accidentally ("rx" instead of "xr")
                self.flats[filt[-1::-1]] = fData
            hdu.close()

    def cycle(self):
        res = self.check_out_files()
        if (self.objName is not None) or (res == 0):
            self.rightPanel.update_object_name(self.objName, self.addString)
            self.run_computation()
        else:
            self.rightPanel.update_object_name("---")
        self.root.after(1500, self.cycle)

    def check_out_files(self):
        allFiles = list(self.dirName.glob("*.FIT"))
        lightFiles = []
        darkFiles = []
        biasFiles = []
        # get rid of dark files and subdirectories
        for f in allFiles:
            fName = f.name
            if ("dark" not in fName) and ("bias" not in fName) and (f.is_file()):
                lightFiles.append(f)
            elif "dark" in fName:
                darkFiles.append(f)
            elif "bias" in fName:
                biasFiles.append(f)
        msg = ""
        if len(biasFiles) < self.camera.biases_required:
            msg += f"Waiting for bias files ({len(biasFiles)}/{self.camera.biases_required})\n"
        if len(darkFiles) < self.camera.darks_required:
            msg += f"Waiting for dark files ({len(darkFiles)}/{self.camera.darks_required})"
        if msg:
            self.imagPanel.show_message(msg)
            return 1
        if len(lightFiles) == 0:
            self.imagPanel.show_message("Waiting for light exposure.")
            return 1

        # Let's find what object is it
        if not self.object_selected_manually:
            newestFile = max(lightFiles, key=lambda x: x.stat().st_ctime)
            fNameWithoutPath = newestFile.name
            self.objName, self.filterCombination, \
                self.addString = self.camera_parser.parse_object_file_name(fNameWithoutPath)

        self.photFiltName, self.polarFiltName = self.camera_parser.get_mode(self.filterCombination)

        if len(lightFiles) and (self.objName is None):
            # Current object do not have a reference
            self.imagPanel.show_message(f"No reference for this object:\n {fNameWithoutPath.stem}")
            return 1

        return 0

    def clean_work_dir(self):
        for fName in self.workdir.glob(f"{self.currentObject}*_affineremap.fits"):
            os.remove(fName)
        for fName in self.workdir.glob(f"{self.currentObject}*.FIT"):
            os.remove(fName)

    def reset_new_filter(self):
        self.rightPanel.update_camera_mode_info(self.photFiltName, self.polarFiltName)
        self.clean_work_dir()
        self.rawImages = []
        self.darkCleanImages = []
        self.magData = []
        self.currentFilterCombination = self.filterCombination
        self.filterChecked = False

    def reset_new_object(self):
        self.reset_new_filter()
        self.currentObject = self.objName
        self.currentAddString = self.addString
        self.ref = alignment.Reference(self.objName, self.camera)
        self.masterBiasData = reduction.make_master_bias(self.dirName, self.camera.biases_required)
        self.masterDarkData, darkNumber, self.hotPixels = reduction.make_master_dark(self.dirName, self.masterBiasData,
                                                                                     self.camera.darks_required)
        objStr = f"{self.objName}:{self.addString}"
        if objStr not in self.photoLog:
            # we only want to add a new object if there is no such
            # object in the dictionary. Otherwise it will erase log data
            # for the current onject if "reset" was called by user.
            self.photoLog[objStr] = {}

        # Clear working directory
        for f in self.workdir.glob("*"):
            if ("dark" not in f.name) and ("bias" not in f.name):
                os.remove(f)

    def update_plot(self):
        self.imagPanel.plot_objects(self.ref, self.polarFiltName, self.hotPixels)

    def run_computation(self):
        """ This is the main function. It is been
        called as soon as the object is selected."""
        if self.filterCombination != self.currentFilterCombination:
            # new filter is being processed
            self.reset_new_filter()

        if (self.objName != self.currentObject) or (self.addString != self.currentAddString):
            # new object is being processed
            self.reset_new_object()

        # Observer can delete some raw files if they are bad,
        # so we need to check if there are files in rawImages list
        # that are not present on HDD anymore and delete them from
        # list and from workDir directory
        imageWasRemoved = False
        for f in self.rawImages:
            if not f.exists():
                imageWasRemoved = True
                self.rawImages.remove(f)
                fName = f.stem
                pathToFile = self.workdir / f"{fName}_affineremap.fits"
                if pathToFile.exists():
                    os.remove(pathToFile)
                pathToFile = self.workdir / f"{fName}.FIT"
                if pathToFile.exists():
                    os.remove(pathToFile)
                if pathToFile in self.darkCleanImages:
                    self.darkCleanImages.remove(pathToFile)

        # Create the list of images to be processed
        newRawImages = []
        # To take into account the possibility that user can accidentally switch the order of filters
        # in the file name (e.g. bllacrx instead of bllacxr) we will perform the searching twice
        filesGrabbed = self.camera_parser.match_files(self.dirName, self.objName, self.addString,
                                                      self.filterCombination)
        for img in sorted(filesGrabbed):
            if img not in self.rawImages:
                newRawImages.append(img)
        if (len(newRawImages) == 0) and (not imageWasRemoved):
            print("no new images")
            return
        self.rawImages.extend(newRawImages)
        print(self.rawImages)

        # Subtract bias and dark files
        if newRawImages:
            flat = self.flats[self.filterCombination.lower()]
            # Perform reduction for biases, darks and flats. Also flip image is needed
            self.newCleanImages, self.biasValue, self.darkValue \
                = reduction.reduction(self.dirName, newRawImages, self.masterDarkData,
                                      self.masterBiasData, flat, self.camera.flip_data_ud,
                                      self.camera.flip_data_lr)
            if not self.newCleanImages:
                return
            self.darkCleanImages.extend(self.newCleanImages)

        # Coadd images
        if self.polarFiltName is None:
            # Camera is not in polar mode. Regular coaddition
            self.numOfCoaddedImages = alignment.coadd_images(self.darkCleanImages, None, camera=self.camera)
        else:
            # Camera is in polar mode
            self.numOfCoaddedImages = alignment.coadd_images(self.darkCleanImages, self.polarFiltName,
                                                             camera=self.camera)
        self.rightPanel.update_number_of_images(self.numOfCoaddedImages)

        # Subtract background
        backData = se.find_background(addString=f" -BACKPHOTO_TYPE {self.params['background_type']} ")
        summedFile = self.workdir / "summed.fits"
        self.imagPanel.plot_fits_file(summedFile)

        # Create a catalogue of objects on summed images
        catName = self.workdir / "field.cat"
        if self.polarFiltName is not None:
            # in polar mode we have two catalogues: polar and filtred one.
            catNamePolar = self.workdir / "field_polar.cat"
            # if there is no given aperture size, we need to run the SExtractor twice:
            # one time to find FWHM and compute aperture, and one time to compute acutal
            # fluxes with this aperture
            if self.ref.apertureSize is None:
                se.call_SE(summedFile, catNamePolar,
                           addString=f"-BACKPHOTO_TYPE {self.params['background_type']}")
                filterPolCat(catNamePolar, catName, self.polarFiltName, self.camera)
                self.seCatPolar = se.SExCatalogue(catNamePolar)
                self.seCat = se.SExCatalogue(catName)

                # Find median FWHM of the image
                medianFWHM = self.seCat.get_median_value("FWHM_IMAGE")

                # Now we want to run SExtractor once again to get fluxes in
                # circular apertures of 1.55*FWHM
                aperRadius = 1.55*medianFWHM+1
            else:
                aperRadius = self.ref.apertureSize
            addString = f"-PHOT_APERTURES {2*aperRadius:1.2f} "
            addString += f"-BACKPHOTO_TYPE {self.params['background_type']}"
            se.call_SE(summedFile, catNamePolar, addString=addString)
            filterPolCat(catNamePolar, catName, self.polarFiltName, self.camera)
            self.seCatPolar = se.SExCatalogue(catNamePolar)
            self.seCat = se.SExCatalogue(catName)

            # Match objects from reference image on the observed one
            returnCode = self.ref.match_objects(summedFile, self.seCatPolar, self.polarFiltName)
        else:
            se.call_SE(summedFile, catName,
                       addString=f"-BACKPHOTO_TYPE {self.params['background_type']}")
            self.seCat = se.SExCatalogue(catName)

            # Match objects from reference image on the observed one
            returnCode = self.ref.match_objects(summedFile, self.seCat)
            if not returnCode:
                # Find mean FWHM of standarts
                meanFWHM = self.ref.get_standatds_fwhm()

                if self.ref.apertureSize is None:
                    # if the aperture size is not given, we want to compute one
                    aperRadius = 1.55*meanFWHM+1
                else:
                    aperRadius = self.ref.apertureSize
                # Now we want to run SExtractor once again to get fluxes in circular apertures
                addString = f"-PHOT_APERTURES {2*aperRadius:1.2f} "
                addString += f"-BACKPHOTO_TYPE {self.params['background_type']}"
                se.call_SE(summedFile, catName, addString=addString)
                self.seCat = se.SExCatalogue(catName)

                # Match objects once again. We already have a valid transform, so
                # we only need to match objects
                returnCode = self.ref.match_objects(summedFile, self.seCat, matchOnly=True)

        if returnCode:
            # matching failed. Maybe we need to coadd more_objects to encrease S/N ratio.
            self.rightPanel.update_message("Matching failed")
            # Remove object marks from image and clean results panel
            self.imagPanel.remove_objects_from_plot(updateFigure=True)
            self.rightPanel.clear_info(["results"])
            return
        else:
            self.rightPanel.update_message("")
        self.update_plot()

        # make aperture photometry of object and standarts
        if self.polarFiltName is None:
            # Camera is in pure photometric mode
            objSn, objMag, objMagSigma, stSn = se.get_photometry_phot_mode(self.seCat, self.ref, self.photFiltName,
                                                                           aperRadius, self.biasValue,
                                                                           self.darkValue, backData)
            self.rightPanel.show_results_photometry(objSn, objMag, objMagSigma, stSn)
            self.magData.append((self.numOfCoaddedImages, objMag))
            # store magnitude value to a photomerty log
            self.photoLog[f"{self.objName}:{self.addString}"][self.filterCombination] = objMag

        elif (self.polarFiltName is not None) and (self.photFiltName is None):
            # Pure polarimetric mode
            objSn, objPairSn, stSn, fluxRatios = se.get_photometry_polar_mode(self.seCatPolar, self.ref, aperRadius,
                                                                              self.biasValue, self.darkValue, backData)
            self.rightPanel.show_results_polarimetry(objSn, objPairSn, stSn, fluxRatios)

        elif (self.polarFiltName is not None) and (self.photFiltName is not None):
            # Mixed polarimetric+photometric mode
            objSn, objMag, objMagSigma, stSn = se.get_photometry_mixed_mode(self.seCatPolar, self.ref,
                                                                            self.photFiltName, aperRadius,
                                                                            self.biasValue, self.darkValue,
                                                                            backData)
            self.rightPanel.show_results_photometry(objSn, objMag, objMagSigma, stSn)
            self.magData.append((self.numOfCoaddedImages, objMag))
            # store magnitude value to a photomerty log
            self.photoLog[f"{self.objName}:{self.addString}"][self.filterCombination] = objMag

        return


MainApplication()
